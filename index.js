const express = require('express')
const app = express()
const crypto = require('crypto')
const secretKey = 'a4384bd541caa78c5137e48ea427f9dcac9000c6e6953ad1717bd603e52b729e';
const bodyParser = require('body-parser')
const cors = require('cors');


// CORS
app.use(cors());

app.use('/api/webhooks', bodyParser.raw({ type: 'application/json' }))
app.use(bodyParser.json())

app.post('/api/webhooks/orders/create', async (req, res) => {
  console.log('We got an order!')

  // we'll compare the hmac to our own hash
  const hmac = req.get('X-Shopify-Hmac-Sha256')

  // create a hash using the body and our key
  const hash = crypto
    .createHmac('sha256', secretKey)
    .update(req.body, 'utf8', 'hex')
    .digest('base64')

  // Compare our hash to Shopify's hash
  if (hash === hmac) {
    // It's a match! All good
    console.log('it came from Shopify!')
    res.sendStatus(200)
  } else {
    // No match! This request didn't originate from Shopify
    console.log('Danger! Not from Shopify!')
    res.sendStatus(403)
  }
})

app.listen(5000, () => console.log('Example app listening on port 5000!'))